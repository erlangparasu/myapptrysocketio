var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);

// app.get('/', (req, res) => {
//   res.send('<h1>Hello world</h1>');
// });

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', (socket) => {
  console.log('on connection');

  socket.on('disconnect', () => {
    console.log('on disconnected');
  });

  socket.on('message', (msg) => {
    console.log('on message: ' + msg);
  });
});

setInterval(() => {
	io.emit('event001', 'tick ' + new Date());
}, 3000);

http.listen(3000, () => {
  console.log('listening on *:3000');
});

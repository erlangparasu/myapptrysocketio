package com.herokuapp.erlangparasu.myapptrysocketio

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*

/**
 * Created by Erlang Parasu on 2020.
 */

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        textOne.append("\n")
//        textOne.append("Begin\n")
//        val socket = IO.socket("http://192.168.43.176:3000")
//        socket.on("event", Emitter.Listener {
//            textOne.append("on event\n")
//        })
//        socket.on(Socket.EVENT_DISCONNECT, Emitter.Listener {
//            textOne.append("on disconnect\n")
//        })
//        socket.on(Socket.EVENT_CONNECT, Emitter.Listener {
//            textOne.append("on connect\n")
//            // socket.emit("foo", "hi")
//        })
//        socket.on(Manager.EVENT_OPEN, Emitter.Listener {
//            textOne.append("on open\n")
//        })
//        socket.connect()
//        textOne.append("Finish.\n")

        textOne.append("\n")
        textOne.append("\n")

        App.getInstance()?.let { app ->
            app.mSubject?.let { subject ->
                subject.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ strData ->
                        textOne.append(strData)
                        textOne.append("\n")
                        textOne.append("\n")
                    }, { exc ->
                        exc.printStackTrace()

                        textOne.append(exc.message)
                        textOne.append("\n")
                        textOne.append("\n")
                    })
            }
        }
    }
}

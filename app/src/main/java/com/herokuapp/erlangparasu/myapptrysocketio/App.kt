package com.herokuapp.erlangparasu.myapptrysocketio

import android.app.Application
import android.util.Log
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import io.socket.client.IO
import io.socket.client.Socket
import io.socket.emitter.Emitter

/**
 * Created by Erlang Parasu on 2020.
 */

class App : Application() {

    val mDisposable: CompositeDisposable = CompositeDisposable()

    var mSubject: PublishSubject<String>? = null

    companion object {

        const val TAG = "App.myTag";

        var sApp: App? = null

        var sSocket: Socket? = null

        @Synchronized
        fun getInstance(): App? {
            return sApp
        }

        @Synchronized
        fun setInstance(app: App) {
            sApp = app
        }

        @Synchronized
        fun getSocket(): Socket? {
            return sSocket
        }

        @Synchronized
        fun setSocket(socket: Socket) {
            sSocket = socket
        }
    }

    override fun onCreate() {
        super.onCreate()

        setInstance(this)

        val subject: PublishSubject<String> = PublishSubject.create()
        mSubject = subject

        val d: Disposable = subject.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ strData ->
                Log.d(TAG, "strData: " + strData)
            }, { exc ->
                exc.printStackTrace()
            })
        mDisposable.add(d)

        subject.onNext("Begin")

        val socket = IO.socket("http://" + getSocketIp() + ":" + getSocketPort() + "")
        setSocket(socket)

        val id = socket.toString()
        socket.on("event001", Emitter.Listener {
            subject.onNext(id + ": " + "on event001: " + it.first().toString())
        })
        socket.on(Socket.EVENT_DISCONNECT, Emitter.Listener {
            subject.onNext(id + ": " + "on disconnect")
        })
        socket.on(Socket.EVENT_CONNECT, Emitter.Listener {
            subject.onNext(id + ": " + "on connect")
            socket.emit("foo", "hi")
        })

        socket.connect()

        subject.onNext(id + ": " + "Finish")
    }

    override fun onLowMemory() {
        mDisposable.clear()
        super.onLowMemory()
    }

    override fun onTerminate() {
        mDisposable.clear()
        super.onTerminate()
    }

    private fun getSocketIp(): String {
        return "192.168.43.176"
    }

    private fun getSocketPort(): String {
        return "3000"
    }
}
